package main.java.com.tsc.apogasiy.tm.repository;


import main.java.com.tsc.apogasiy.tm.api.repository.ICommandRepository;
import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    @NotNull private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return arguments.get(arg);
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.getCommand();
            if (name.isEmpty())
                continue;
            result.add(name);
        }
        return result;
    }

    @Override
    @NotNull
    public Collection<String> getArgNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String arg = command.getArgument();
            if (arg == null || arg.isEmpty())
                continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.getArgument();
        @NotNull final String name = command.getCommand();
        if (arg != null)
            arguments.put(arg, command);
        commands.put(name, command);
    }

}
