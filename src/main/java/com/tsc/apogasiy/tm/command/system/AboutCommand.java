package main.java.com.tsc.apogasiy.tm.command.system;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AboutCommand extends AbstractCommand {

    @Override
    public @NotNull String getCommand() {
        return "about";
    }

    @Override
    public @Nullable String getArgument() {
        return "-a";
    }

    @Override
    public @NotNull String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("Developer: Alexey Pogasiy");
        System.out.println("e-mail: apogasiy@tsconsulting.com");
    }

}
