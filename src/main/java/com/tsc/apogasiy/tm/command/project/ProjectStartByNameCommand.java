package main.java.com.tsc.apogasiy.tm.command.project;

import main.java.com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-start-by-name";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Start project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(userId, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        serviceLocator.getProjectService().startByName(userId, name);
    }

}
