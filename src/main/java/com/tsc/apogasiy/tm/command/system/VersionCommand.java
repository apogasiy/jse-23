package main.java.com.tsc.apogasiy.tm.command.system;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VersionCommand extends AbstractCommand {

    @Override
    public @NotNull String getCommand() {
        return "version";
    }

    @Override
    public @Nullable String getArgument() {
        return "-v";
    }

    @Override
    public @NotNull String getDescription() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println("1.17.0");
    }

}
