package main.java.com.tsc.apogasiy.tm.command.user;

import main.java.com.tsc.apogasiy.tm.command.AbstractUserCommand;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyUserListException;
import main.java.com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import main.java.com.tsc.apogasiy.tm.model.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UserListCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-list";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Display list of users";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin)
            throw new AccessDeniedException();
        System.out.println("User list");
        final List<User> users = serviceLocator.getUserService().findAll();
        if (users == null)
            throw new EmptyUserListException();
        for (final User user : users)
            showUser(user);
    }

}
