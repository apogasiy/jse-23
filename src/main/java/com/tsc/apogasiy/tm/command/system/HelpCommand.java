package main.java.com.tsc.apogasiy.tm.command.system;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HelpCommand extends AbstractCommand {

    @Override
    public @NotNull String getCommand() {
        return "help";
    }

    @Override
    public @Nullable String getArgument() {
        return "-h";
    }

    @Override
    public @NotNull String getDescription() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands())
            System.out.println(command.toString());
    }

}
