package main.java.com.tsc.apogasiy.tm.command.auth;

import main.java.com.tsc.apogasiy.tm.command.AbstractAuthCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class LogoffCommand extends AbstractAuthCommand {

    @Override
    public @NotNull String getCommand() {
        return "logoff";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "User logoff from system";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

}
