package main.java.com.tsc.apogasiy.tm.command.user;

import main.java.com.tsc.apogasiy.tm.command.AbstractUserCommand;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserCreateCommand extends AbstractUserCommand {
    @Override
    public @NotNull String getCommand() {
        return "user-create";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password: ");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email: ");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().create(login, password, email);
    }

}
