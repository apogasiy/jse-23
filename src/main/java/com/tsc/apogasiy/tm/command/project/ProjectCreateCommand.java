package main.java.com.tsc.apogasiy.tm.command.project;

import main.java.com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-create";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(userId, name, description);
    }

}
