package main.java.com.tsc.apogasiy.tm.api.repository;

import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectRepository extends IOwnerRepository<Project> {

    boolean existsByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Project findByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Project removeByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Project startById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Project startByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    Project startByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Project finishById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Project finishByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    Project finishByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status);

    @Nullable
    Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status);

    @Nullable
    Project changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status);

}
