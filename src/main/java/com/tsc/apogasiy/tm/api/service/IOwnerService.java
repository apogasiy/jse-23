package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@Nullable final String userId);

    @NotNull
    List<E> findAll(@Nullable final String userId, @NotNull final Comparator<E> comparator);

    @Nullable
    E findById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    E findByIndex(@Nullable final String userId, @Nullable final Integer index);

    void clear(@Nullable final String userId);

    void remove(@Nullable final String userId, final E entity);

    @NotNull
    Integer getSize(@NotNull final String userId);

}
