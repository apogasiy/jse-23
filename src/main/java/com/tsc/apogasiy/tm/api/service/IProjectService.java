package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectService extends IOwnerService<Project> {

    void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description);

    @Nullable
    Project findByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Project findByIndex(@Nullable final String userId, @Nullable final Integer index);

    @Nullable
    Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description);

    @Nullable
    Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description);

    boolean existsByIndex(@Nullable final String userId, @NotNull final Integer index);

    boolean existsByName(@Nullable final String userId, @NotNull final String name);

    @Nullable
    Project startById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Project startByIndex(@Nullable final String userId, @Nullable final Integer index);

    @Nullable
    Project startByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Project finishById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Project finishByIndex(@Nullable final String userId, @Nullable final Integer index);

    @Nullable
    Project finishByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status);

    @Nullable
    Project changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status);

    @Nullable
    Project changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status);

}
