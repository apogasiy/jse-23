package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @Nullable String getCurrentUserId();

    void setCurrentUserId(@NotNull final String userId);

    boolean isAuth();

    boolean isAdmin();

    void login(@Nullable final String login, @Nullable final String password);

    void logout();

    void checkRoles(@Nullable final Role... roles);

}
