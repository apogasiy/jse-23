package main.java.com.tsc.apogasiy.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        @NotNull final String value = SCANNER.nextLine();
        return Integer.parseInt(value);
    }

}