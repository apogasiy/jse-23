package main.java.com.tsc.apogasiy.tm.exception.system;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@NotNull final String command) {
        super("Incorrect command:'" + command + "'");
    }

}
