package main.java.com.tsc.apogasiy.tm.exception.entity;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UserEmailExistsException extends AbstractException {

    public UserEmailExistsException(@NotNull final String email) {
        super("Error! User with email '" + email + "' already exists.");
    }

}
