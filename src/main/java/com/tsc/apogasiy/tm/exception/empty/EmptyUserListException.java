package main.java.com.tsc.apogasiy.tm.exception.empty;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyUserListException extends AbstractException {

    public EmptyUserListException() {
        super("Error! User list is empty!");
    }

}
