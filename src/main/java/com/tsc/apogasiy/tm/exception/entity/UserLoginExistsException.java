package main.java.com.tsc.apogasiy.tm.exception.entity;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UserLoginExistsException extends AbstractException {

    public UserLoginExistsException(@NotNull final String login) {
        super("Error! User with login '" + login + "' already exists.");
    }

}
