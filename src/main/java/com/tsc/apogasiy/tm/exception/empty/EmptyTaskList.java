package main.java.com.tsc.apogasiy.tm.exception.empty;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyTaskList extends AbstractException {

    public EmptyTaskList() {
        super("Error! Task list is empty!");
    }

}
