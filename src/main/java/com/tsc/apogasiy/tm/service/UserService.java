package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IUserRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IUserService;
import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.exception.empty.*;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserEmailExistsException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserLoginExistsException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final IUserRepository userRepository;

    public UserService(@NotNull IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        if (isLoginExists(login))
            throw new UserLoginExistsException(login);
        @NotNull final User user = new User(login, HashUtil.encrypt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable String login, @Nullable String password, @NotNull String email) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable String login, @Nullable String password, @NotNull Role role) {
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    @NotNull
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setPassword(HashUtil.encrypt(password));
        return user;
    }

    @Override
    @NotNull
    public User setRole(@Nullable final String userId, @Nullable final Role role) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyIdException();
        if (role == null)
            throw new EmptyRoleException();
        @NotNull final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return user;

    }

    @Override
    public void remove(@NotNull User user) {
        userRepository.remove(user);
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    public User findByEmail(@Nullable String email) {
        if (!Optional.ofNullable(email).isPresent() || email.isEmpty())
            throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    @Nullable
    public User removeByLogin(@Nullable final String login) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(@NotNull String login) {
        return userRepository.findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    @NotNull
    public User updateById(@Nullable final String id, @Nullable final String lastName, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String email) {
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        if (!Optional.ofNullable(lastName).isPresent() || lastName.isEmpty())
            throw new EmptyLastNameException();
        if (!Optional.ofNullable(firstName).isPresent() || firstName.isEmpty())
            throw new EmptyFirstNameException();
        if (!Optional.ofNullable(middleName).isPresent() || middleName.isEmpty())
            throw new EmptyMiddleNameException();
        if (!Optional.ofNullable(email).isPresent() || email.isEmpty())
            throw new EmptyEmailException();
        if (isEmailExists(email))
            throw new UserEmailExistsException(email);
        @NotNull final User user = findById(id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User updateByLogin(@Nullable final String login, @Nullable final String lastName, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String email) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        if (!Optional.ofNullable(lastName).isPresent() || lastName.isEmpty())
            throw new EmptyLastNameException();
        if (!Optional.ofNullable(firstName).isPresent() || firstName.isEmpty())
            throw new EmptyFirstNameException();
        if (!Optional.ofNullable(middleName).isPresent() || middleName.isEmpty())
            throw new EmptyMiddleNameException();
        if (!Optional.ofNullable(email).isPresent() || email.isEmpty())
            throw new EmptyEmailException();
        if (isLoginExists(email))
            throw new UserLoginExistsException(login);
        @NotNull final User user = findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        return user;
    }

    @Override
    @NotNull
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        return user;
    }

}
