package main.java.com.tsc.apogasiy.tm.service;

import lombok.RequiredArgsConstructor;
import main.java.com.tsc.apogasiy.tm.api.repository.IAuthRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IAuthService;
import main.java.com.tsc.apogasiy.tm.api.service.IUserService;
import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyIdException;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyLoginException;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyPasswordException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

@RequiredArgsConstructor
public class AuthService implements IAuthService {

    @Nullable private final IAuthRepository authRepository;
    @Nullable private final IUserService userService;

    @Override
    public String getCurrentUserId() {
        @NotNull final String userId = authRepository.getCurrentUserId();
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        return userId;
    }

    @Override
    public void setCurrentUserId(@NotNull final String userId) {
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public boolean isAuth() {
        @Nullable final String currentUserId = authRepository.getCurrentUserId();
        return (Optional.ofNullable(currentUserId).isPresent() || !currentUserId.isEmpty());
    }

    @Override
    public boolean isAdmin() {
        @Nullable final Role role = userService.findById(getCurrentUserId()).getRole();
        return role.equals(Role.ADMIN);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        if (user.getLocked())
            throw new AccessDeniedException();
        final String hash = HashUtil.encrypt(password);
        if (!hash.equals(user.getPassword()))
            throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (!isAuth())
            throw new AccessDeniedException();
        setCurrentUserId(null);
    }

    @Override
    public void checkRoles(final Role... roles) {
        if (roles == null || roles.length == 0)
            return;
        @NotNull final User user = userService.findById(getCurrentUserId());
        if (user == null)
            throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        if (role == null)
            throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role))
                return;
        }
        throw new AccessDeniedException();
    }

}
