package main.java.com.tsc.apogasiy.tm.service;

import lombok.RequiredArgsConstructor;
import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IProjectTaskService;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class ProjectTaskService implements IProjectTaskService {

    @Nullable private final ITaskRepository taskRepository;
    @Nullable private final IProjectRepository projectRepository;

    @Override
    @Nullable
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (!Optional.ofNullable(projectId).isPresent() || projectId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    @Nullable
    public Task bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (!Optional.ofNullable(projectId).isPresent() || projectId.isEmpty()) return null;
        if (!Optional.ofNullable(taskId).isPresent() || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    @Nullable
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (!Optional.ofNullable(projectId).isPresent() || projectId.isEmpty()) return null;
        if (!Optional.ofNullable(taskId).isPresent() || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    @Nullable
    public Project removeById(@NotNull final String userId, @NotNull final String projectId) {
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    @Nullable
    public Project removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Project project = projectRepository.findByIndex(index);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    @Nullable
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = projectRepository.findByName(userId, name);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

}
