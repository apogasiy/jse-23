package main.java.com.tsc.apogasiy.tm.model;

import lombok.Getter;
import lombok.Setter;
import main.java.com.tsc.apogasiy.tm.api.entity.IWBS;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class Project extends AbstractOwnerEntity implements IWBS {

    @Getter @Setter @NotNull private String name;
    @Getter @Setter @Nullable private String description;
    @Getter @Setter @Nullable private Status status = Status.NOT_STARTED;
    @Getter @Setter @Nullable private Date startDate = null;
    @Getter @Setter @Nullable private Date created = new Date();

    public Project(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name;
    }
}
