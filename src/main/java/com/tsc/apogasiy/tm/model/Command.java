package main.java.com.tsc.apogasiy.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@AllArgsConstructor
public class Command {

    @Getter @NotNull private final String name;
    @Getter @Nullable private final String argument;
    @Getter @Nullable private final String description;

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        if (name != null && !name.isEmpty())
            result += name + " ";
        if (argument != null && !argument.isEmpty())
            result += "(" + argument + ") ";
        if (description != null && !description.isEmpty())
            result += description;
        return result;
    }
}
